Dataset was taken from https://www.kaggle.com/kushleshkumar/cornell-car-rental-dataset for learning purpose. 


### How to start:
Download dataset from https://www.kaggle.com/kushleshkumar/cornell-car-rental-dataset to file CatRentalData.csv
```
pip install -r requirements
streamlit run demo.py
```
